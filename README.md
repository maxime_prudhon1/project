## Data mining Project 

## I. Purpose of the project
The objective of this project was to recommend images to users according to their
preferences. For this, users will give their opinions on different photos and the
program will automatically suggest photos likely to match
their tastes.

## II. Sources of image data and their license.
Images are downloaded when running the script through the Unsplash site API
bringing together a large collection of images. All Unsplash photos are made for
be used freely. Unsplash grants you an irrevocable copyright license,
non-exclusive, worldwide license to download, copy, modify, distribute, perform and
use photos found on their site for free, including for commercial purposes,
without the permission or attribution of the photographer or Unsplash. This license does not include
however not the right to compile photos of Unsplash to reproduce a service
similar or competitive.

## III. Size of your data
In order to give the user a choice, we have chosen to give him the possibility of
modify some variables like:
“queryThemes”: list of image themes
“imgQuantity”: number of images to download per theme present in “queryThemes”
“choiceNumber”: corresponds to the number of images that will be offered to the user
The size of the sample will therefore depend on the number of themes present in the variable
“queryThemes”

## IV. Information you have decided to store for each image
For each image we stored:

A login
A name
The creation date
The width
The height
The names of the three most common colors
The first three colors of the image as RGB
The image URL
A brief description
The name of the user who posted the photo
The orientation of the photo (landscape, portrait, square)
One or more tag(s)

## V. Information regarding user preferences
Information related to user preferences is:

An id (that of the user)
The colors present in the images he liked
The colors present in the images that he did not like
The list of images he liked - The list of images he disliked - His orientation
favorite image
The tags linked to the images he liked

## VI. Data mining models and/or
of machine learning that you used with the obtained metrics
Based on the user's choices and preferences, the algorithm determines which
images would be likely to please the user.
At first the algorithm will highlight all the images having the orientation
user's favorite by the full list of images → this is list 01.
Secondly, this same algorithm will come out from “list 01” every
images with one of the user's favorite colors → here the list 02.
Finally the algorithm will search in list 02 for all images with one of the tags
preferred by the user: the results will finally be displayed to him
